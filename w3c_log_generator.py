
"""
@author: alexnl022
@date: 18/03/2017
@see: Create a fake log generator
"""

from faker import Faker
import random
import sys
import datetime
import time
from optparse import OptionParser

parser = OptionParser("usage: %prog [options]")
parser.add_option("-o", "--outputfile",
                  help="Path to file log, default is test.log",
                  action="store", default='test.log',
                  dest="output")
parser.add_option("-c", "--write_to_console",
                  help="Write the output to console",
                  action="store_true", default=False,
                  dest="console")
parser.add_option("-n", "--n",
                  help="Erease the file to write it from zero",
                  action="store_true", default=False,
                  dest="new")

(options, args) = parser.parse_args()

fake = Faker()
cs_uris=[]

for i in range(100):
    cs_uris.append(fake.uri_path())

cs_methods = ['GET','HEAD','POST','OPTION','CONNECT','TRACE','PUT','PATCH','DELETE']

if options.console:
    f = sys.stdout
    f.write("#Fields: time cs-method cs-uri c-ip\n")
    f.write("#Version: 1.0\n")
elif options.new :
    f = open(options.output,'w+')
    f.write("#Fields: time cs-method cs-uri c-ip\n")
    f.write("#Version: 1.0\n")
    f.close()

while True:
    cs_method = random.choices(cs_methods,[0.5,0.02,0.1,0.02,0.02,0.02,0.2,0.02,0.1])[0]
    cs_uri = random.choice(cs_uris)
    time_request = datetime.datetime.utcnow().time()
    c_ip = fake.ipv4()

    # a+ is to write at the end of the fil
    if options.console:
        f = sys.stdout
    else:
        f = open(options.output, 'a+')
    f.write('%s %s %s %s\n' % (time_request, cs_method, cs_uri, c_ip))

    if not options.console:
        f.close()

    time.sleep(random.uniform(0.0, 2))